window.onload = () => {
  setTimeout(() => {
    document.querySelector('body').classList.add('display');
  }, 250);
};

document.querySelector('.hamburger-menu').addEventListener('click', () => {
  document.querySelector('.container').classList.toggle('change');
});

//transform 1st and 3rd line -- hide second line

document.querySelector('.scroll-btn').addEventListener('click', () => {
  document.querySelector('html').style.scrollBehavior = 'smooth';
  // you have to remove smooth scrolling or will be on every button
  setTimeout(() => {
    document.querySelector('html').style.scrollBehavior = 'unset';
  }, 1000);
});
